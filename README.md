[![Build Status](https://travis-ci.org/fennecdjay/gwion-ast.svg?branch=master)](https://travis-ci.org/fennecdjay/gwion-ast)
[![BCH compliance](https://bettercodehub.com/edge/badge/fennecdjay/gwion-ast?branch=master)](https://bettercodehub.com/)

# Gwion-AST

Gwion language lexer, parser, preprocessor and abstract syntatxic tree.  
Meant to be be used as a submodule and ease Tooling
